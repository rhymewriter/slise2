package services;

public class SortingService {

    public int[] quickSorting(int[] array, int lowInd, int highInd) {
        if (array == null || highInd > array.length - 1 || lowInd < 0) {
            return null;
        }

        if (array.length == 0) {
            return array;
        }


        if (lowInd >= highInd) {
            return array;
        }


        int mid = array[lowInd + (highInd - lowInd) / 2];


        int i = lowInd;
        int j = highInd;
        while (i <= j) {

            while (array[i] < mid) {
                i++;
            }


            while (array[j] > mid) {
                j--;
            }


            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }


        if (lowInd < j) {
            quickSorting(array, lowInd, j);
        }

        if (highInd > i) {
            quickSorting(array, i, highInd);
        }

        return array;
    }
}
